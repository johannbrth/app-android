# [ANDROID] Take/import a photo and send it to Parse #

With this application, you can take a photo or import one from your own gallery. 
Then, the application sends it on the online service [Parse.com](https://www.parse.com/) which will stock it in a database.

## Introduction ##

* Project : Big Brother
* Author : Johann B.
* Software : Android Studio 1.3.2
* Application : API 23: Android 6.0

## How do I run this app? ##

* Step 1 : download these sources in a repository
* Step 2 : open Android Studio, file -> open and select your repository
* Step 3 : run the app by selecting the button 'Run' (or press Maj+F10)

## How do I link my app with Parse? ##

```

	Parse.initialize(this, "first_key", "second_key");
    
```
You can replace it with our own key, in the StarterApplication.java file.
You just have to create on Parse a new project.

## How do I send data to Parse? ##

```

  ParseObject targetObject = new ParseObject("target");
  targetObject.put("name", "test"); //add an attribute 'name' with the value 'test'
  targetObject.put("picture", photoFile); //add an attribute 'picture' with the file 'photoFile'
  targetObject.saveInBackground();
  
```


## How do I send data to Parse? ##

```

	ByteArrayOutputStream stream = new ByteArrayOutputStream();
    // Compress image to lower quality scale 1 - 100
    thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
    byte[] image = stream.toByteArray();
    ParseFile photoFile = new ParseFile("portrait.JPEG", image);
    targetObject.put("picture", photoFile);
    
```
Before this code, you must have an image 'thumbnail' in bitmap format.

## What has already been done? ##

* Take a photo / Import
* Display it
* Send it to Parse
	  
## To Do ##

* Include an API to manage geolocalisation
* Send back a notification to notify the user that the server is dealing with his file

## To Fix ##

* Nothing, for the moment...

## API to plug ##

* API guide : https://www.parse.com/docs/rest/guide
* Base URL : https://api.parse.com/1/
* API get example: https://api.parse.com/1/classes/image

```

 GET params :
 X-Parse-Application-Id: DPCNqaHZAnsvNW7DK0eANzg0vjMyoAabkt87WvIC
 X-Parse-REST-API-Key: lgt37WQcVF94FLka4YjbZEwc6ldAfyivsmqpUo4Y
 Response:
 {"results":[{"coordonees":"30°15 N ; 15°00 E","createdAt":"2015-09-10T07:47:04.732Z","date":{"__type":"Date","iso":"2015-09-10T07:46:00.000Z"},"objectId":"3eidPWluTc","updatedAt":"2015-09-10T07:47:32.417Z"},{"coordonees":"","createdAt":"2015-09-10T08:02:51.940Z","date":{"__type":"Date","iso":"2015-08-18T20:46:00.000Z"},"objectId":"8zPq8vEJbd","updatedAt":"2015-09-10T08:02:51.940Z"},{"coordonees":"Paris","createdAt":"2015-09-10T08:05:58.738Z","date":{"__type":"Date","iso":"2015-07-14T22:30:00.000Z"},"objectId":"L8fZ9o72DP","updatedAt":"2015-09-10T08:05:58.738Z"}]}
 API Post exemple
 https://api.parse.com/1/classes/image
 POST
 X-Parse-Application-Id: DPCNqaHZAnsvNW7DK0eANzg0vjMyoAabkt87WvIC
 X-Parse-REST-API-Key: lgt37WQcVF94FLka4YjbZEwc6ldAfyivsmqpUo4Y
 {
	"date": {
		"__type": "Date",
		"iso": "2015-07-14T22:30:00.000Z"
	},
	"coordonees":"Paris"
 }
 
```