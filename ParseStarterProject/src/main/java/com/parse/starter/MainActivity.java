/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.parse.ParseAnalytics;
import com.parse.ParseFile;
import com.parse.ParseObject;

import java.io.ByteArrayOutputStream;


public class MainActivity extends ActionBarActivity {

  Button btnTakePhoto; //du bouton pour prendre une photo
  Button btnDLPhoto; //du bouton pour importer une image de la galerie
  Button btnSend; //du bouton pour envoyer les datas au serveur

  ImageView imgTakenPhoto; // de l'emplacement où l'aperçu va apparaître
  Bitmap thumbnail; // de l'aperçu de la photo prise

  private static final int CAM_REQUEST = 1313;
  private static final int GAL_REQUEST = 2;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    //Liaison des objets déclarés avec la vue via leur id
    btnTakePhoto = (Button) findViewById(R.id.button_take);
    btnDLPhoto = (Button) findViewById(R.id.button_import);
    imgTakenPhoto = (ImageView) findViewById(R.id.imageView);
    btnSend = (Button) findViewById(R.id.button_send);

    //Ajoute d'un listener sur chaque bouton
    btnTakePhoto.setOnClickListener(new btnTakePhotoClicker());
    btnDLPhoto.setOnClickListener(new btnDLPhotoClicker());
    btnSend.setOnClickListener(new btnSendClicker());

    ParseAnalytics.trackAppOpenedInBackground(getIntent());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    //Cette condition nous permet d'exécuter le code si dessous seulement si l'activité
    //caméra est lancée et si une photo a bien été prise.
    //Avec cette condition, on eput quitter l'appareil photo sans avoir pris de photo
    // et revenir sur l'appli sans que celle-ci ne crash.
    if(requestCode == CAM_REQUEST && resultCode == RESULT_OK) {

      if (thumbnail != null) {
        thumbnail.recycle(); //si un aperçu est déjà présent, il est supprimé
      }

      //Création d'un nouvel aperçu
      Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

      Bitmap pic = thumbnail.copy(thumbnail.getConfig(), true);
      thumbnail.recycle();
      thumbnail = pic;

      imgTakenPhoto.setImageBitmap(thumbnail);

      ParseObject targetObject = new ParseObject("target");
      targetObject.put("name", "Test");
      // Save the scaled image to Parse
      //ParseFile photoFile = new ParseFile("meal_photo.jpg", pic);

      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      // Compress image to lower quality scale 1 - 100
      thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, stream);
      byte[] image = stream.toByteArray();
      ParseFile photoFile = new ParseFile("portrait.JPEG", image);
      targetObject.put("picture", photoFile);
      targetObject.saveInBackground();


      /*SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss"); //formatage date + heure
      Calendar calendar = new GregorianCalendar(); //récupération de la date + heure courantes
      System.out.println(sdf.format(calendar.getTime()));
      TextView dateView = (TextView) findViewById(R.id.dateView);
      dateView.setText("Date : " + String.valueOf(sdf.format(calendar.getTime())));*/
    }
    else if (requestCode == GAL_REQUEST && resultCode == RESULT_OK) {

      //Selection de l'image souhaitée pour l'aperçu
      Uri selectedImage = data.getData();

      String[] filePath = { MediaStore.Images.Media.DATA };

      //Redimensionnement de l'image sélectionnée
      Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

      c.moveToFirst();

      int columnIndex = c.getColumnIndex(filePath[0]);

      String picturePath = c.getString(columnIndex);

      //Fermeture du gestionnaire de photos
      c.close();

      //Création d'un nouvel aperçu
      Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

      //on met l'image dans cet aperçu
      imgTakenPhoto.setImageBitmap(thumbnail);

    }
  }

  //Listener du bouton pour appeler la caméra
  class btnTakePhotoClicker implements Button.OnClickListener {

    @Override
    public void onClick(View v) {
      //Suite au clic sur le bouton correspondant
      //on défini un intent correspondant à l'application caméra
      //et on appele la fonction permettant de créer l'aperçu
      Intent cameraintent = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
      startActivityForResult(cameraintent, CAM_REQUEST);
    }
  }

  //Listener du bouton pour ouvrir le gestionnaire d'images
  class btnDLPhotoClicker implements Button.OnClickListener {

    @Override
    public void onClick(View v) {
      //Suite au clic sur le bouton correspondant
      //on défini un intent correspondant au gestionnaire d'images
      //et on appele la fonction permettant de créer l'aperçu
      Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
      startActivityForResult(intent, GAL_REQUEST);
    }
  }

  //Listener du bouton pour envoyer les datas au serveur
  class btnSendClicker implements Button.OnClickListener {

    @Override
    public void onClick(View v) {
      //code à venir
    }
  }
}
