package com.example.johann.appli_camera;

/**
 * Application Mobile
 *
 * Réalisé par:
 * Berthet Johann
 *
 */

//Imports
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.telephony.TelephonyManager;
import android.view.View;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;


public class MainActivity extends ActionBarActivity {

    //Déclaration...
    Button btnTakePhoto; //du bouton pour prendre une photo
    Button btnDLPhoto; //du bouton pour importer une image de la galerie
    Button btnSend; //du bouton pour envoyer les datas au serveur

    ImageView imgTakenPhoto; // de l'emplacement où l'aperçu va apparaître
    Bitmap thumbnail; // de l'aperçu de la photo prise

    /*TelephonyManager telephonyManager = (TelephonyManager)getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
    String tmDevice = "" + telephonyManager.getDeviceId();
    String tmSerial = "" + telephonyManager.getSimSerialNumber();
    String androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
    String deviceId = deviceUuid.toString();*/

    private static final int CAM_REQUEST = 1313;
    private static final int GAL_REQUEST = 2;

    /**
     * CONSTRUCTEUR
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Liaison des objets déclarés avec la vue via leur id
        btnTakePhoto = (Button) findViewById(R.id.button_take);
        btnDLPhoto = (Button) findViewById(R.id.button_import);
        imgTakenPhoto = (ImageView) findViewById(R.id.imageView);
        btnSend = (Button) findViewById(R.id.button_send);

        //Ajoute d'un listener sur chaque bouton
        btnTakePhoto.setOnClickListener(new btnTakePhotoClicker());
        btnDLPhoto.setOnClickListener(new btnDLPhotoClicker());
        btnSend.setOnClickListener(new btnSendClicker());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //Cette condition nous permet d'exécuter le code si dessous seulement si l'activité
        //caméra est lancée et si une photo a bien été prise.
        //Avec cette condition, on eput quitter l'appareil photo sans avoir pris de photo
        // et revenir sur l'appli sans que celle-ci ne crash.
        if(requestCode == CAM_REQUEST && resultCode == RESULT_OK) {

            if (thumbnail != null) {
                thumbnail.recycle(); //si un aperçu est déjà présent, il est supprimé
            }

            //Création d'un nouvel aperçu
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");

            Bitmap pic = thumbnail.copy(thumbnail.getConfig(), true);
            thumbnail.recycle();
            thumbnail = pic;

            imgTakenPhoto.setImageBitmap(thumbnail);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss"); //formatage date + heure
            Calendar calendar = new GregorianCalendar(); //récupération de la date + heure courantes

            System.out.println(sdf.format(calendar.getTime()));
            //System.out.println(deviceId);

            TextView dateView = (TextView) findViewById(R.id.dateView);
            dateView.setText("Date : " + String.valueOf(sdf.format(calendar.getTime())));
        }
        else if (requestCode == GAL_REQUEST && resultCode == RESULT_OK) {

            //Selection de l'image souhaitée pour l'aperçu
            Uri selectedImage = data.getData();

            String[] filePath = { MediaStore.Images.Media.DATA };

            //Redimensionnement de l'image sélectionnée
            Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

            c.moveToFirst();

            int columnIndex = c.getColumnIndex(filePath[0]);

            String picturePath = c.getString(columnIndex);

            //Fermeture du gestionnaire de photos
            c.close();

            //Création d'un nouvel aperçu
            Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));

            //on met l'image dans cet aperçu
            imgTakenPhoto.setImageBitmap(thumbnail);

        }
    }

    //Listener du bouton pour appeler la caméra
    class btnTakePhotoClicker implements Button.OnClickListener {

        @Override
        public void onClick(View v) {
            //Suite au clic sur le bouton correspondant
            //on défini un intent correspondant à l'application caméra
            //et on appele la fonction permettant de créer l'aperçu
            Intent cameraintent = new Intent (android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraintent, CAM_REQUEST);
        }
    }

    //Listener du bouton pour ouvrir le gestionnaire d'images
    class btnDLPhotoClicker implements Button.OnClickListener {

        @Override
        public void onClick(View v) {
            //Suite au clic sur le bouton correspondant
            //on défini un intent correspondant au gestionnaire d'images
            //et on appele la fonction permettant de créer l'aperçu
            Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, GAL_REQUEST);
        }
    }

    //Listener du bouton pour envoyer les datas au serveur
    class btnSendClicker implements Button.OnClickListener {

        @Override
        public void onClick(View v) {
            //code à venir
        }
    }
}
